#include <iostream>
#include <string>
#include <cmath>
#include <ctime>
#include <stdlib.h>
using namespace std;

int main() {
	// Organisms and their percentages
	string organism[] = {"Chicken", "E. coli", "Grasshopper", "Human", "Maize", "Octopus", "Rat", "Sea Urchin", "Wheat", "Yeast"};
	double perc_A[] = {28.0, 24.7, 29.3, 29.3, 26.8, 33.2, 28.6, 32.8, 27.3, 31.3};
	double perc_G[] = {22.0, 26.0, 20.5, 20.7, 22.8, 17.6, 21.4, 17.7, 22.7, 18.7};
	double perc_C[] = {21.6, 25.7, 20.7, 20.0, 23.2, 17.6, 20.5, 17.3, 22.8, 17.1};
	double perc_T[] = {28.4, 23.6, 29.3, 30.0, 27.2, 31.6, 28.4, 32.1, 27.1, 32.9};
	int size_a, size_g, size_c, size_t;
	srand( (unsigned)time(NULL) );
	// Show options
	for (int i = 0; i < 10; i++) {
		cout << i + 1 << ". " << organism[i] << endl;
	}
	// Ask for an option
	int option;
	cout << "Please enter organism type: ";
	cin >> option;
	double actual_a = 0, actual_g = 0, actual_c = 0, actual_t = 0;
	bool A = true, G = true, C = true, T = true;
	int count_a = 0, count_g = 0, count_c = 0, count_t = 0;
	double length = 0;
	string seq = "";
	// Check valid option
	if (option >= 1 && option <= 10) {
		cout << endl << organism[option - 1] << " is selected." << endl;
		cout << "Please enter the sequence length: ";
		cin >> length;
		
		int i = 0;
		// Calculate the number of A present in the sequence for an organism and round it
		size_a = round(perc_A[option - 1] /100.0 * length);
		size_g = round(perc_G[option - 1] /100.0 * length);
		size_c = round(perc_C[option - 1] /100.0 * length);
		size_t = round(perc_T[option - 1] /100.0 * length);
		
		// While the counters are not equal to the sizes, continue adding to the sequence
		while (count_a != size_a && count_g != size_g && count_c != size_c && count_t != size_t) {
			// Random between 0 - 3
			int result = (rand() % 4);
			// If there are A availables
			if (result == 0 && count_a != size_a) {
				seq += "A";
				count_a += 1;
			}
			// If there are G availables
			else if (result == 1 && count_g != size_g) {
				seq += "G";
				count_g += 1;
			}
			// If there are C availables
			else if (result == 2 && count_c != size_c) {
				seq += "C";
				count_c += 1;
			}
			// If there are T availables
			else if (result == 3 && count_t != size_t) {
				seq += "T";
				count_t += 1;
			}
		}
		
	}
	cout << "Here is the output: " << seq << endl;
	cout << "Analysis Result: " << size_a * 100 / length << "% A " << size_t * 100 / length << "% T " << size_c * 100 / length << "% C " << size_g * 100 / length << "% G" << endl;
	return 0;
}

